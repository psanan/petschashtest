static char help[] = "PETSc program to test simple hashing\n\n";

  /* note: that the PetscHashIXXX "calls" look bizarre, but they 
     are in fact macros so you can pass names of variables to be 
     assigned to, not addresses, e.g. &val, like you would be forced 
     to do for a real C function. They also don't return error
     codes, which also looks strange in PETSc code. */

#include "petsc.h"
#include "../src/sys/utils/hash.h" /* This will move in PETSc 3.8 */

int main(int argc, char **argv)
{
  PetscErrorCode ierr;
  PetscHashI     hash;
  PetscInt       val;
  PetscBool      flg;

  ierr = PetscInitialize(&argc,&argv,(char*)0,help);CHKERRQ(ierr);

  PetscHashICreate(hash);

  ierr = PetscPrintf(PETSC_COMM_WORLD,"Adding 17-->22, 37-->-1\n");CHKERRQ(ierr);
  PetscHashIAdd(hash,17,22);
  PetscHashIAdd(hash,37,-1); /* Note that -1 is also the "not found" value,
                                so don't forget to check if the key exists! 
                                (or live fast and dangerous if you know your 
                                values are all non-negative) */

  PetscHashIHasKey(hash,17,flg);
  if (flg) {
    PetscHashIMap(hash,17,val);
    ierr = PetscPrintf(PETSC_COMM_WORLD,"17 --> %d\n",val);CHKERRQ(ierr);
  } else {
    ierr = PetscPrintf(PETSC_COMM_WORLD,"17 --> not found %d\n");CHKERRQ(ierr);
  }

  PetscHashIHasKey(hash,37,flg);
  if (flg) {
    PetscHashIMap(hash,37,val);
    ierr = PetscPrintf(PETSC_COMM_WORLD,"37 --> %d\n",val);CHKERRQ(ierr);
  } else {
    ierr = PetscPrintf(PETSC_COMM_WORLD,"37 --> not found %d\n");CHKERRQ(ierr);
  }

  PetscHashIHasKey(hash,29,flg);
  if (flg) {
    PetscHashIMap(hash,29,val);  /* would set val = -1 if executed */
    ierr = PetscPrintf(PETSC_COMM_WORLD,"29 --> %d\n",val);CHKERRQ(ierr);
  } else {
    ierr = PetscPrintf(PETSC_COMM_WORLD,"29 --> (not found)\n");CHKERRQ(ierr);
  }

  PetscHashIDestroy(hash);

  ierr = PetscFinalize();
  return 0;
}
